# Go REST API with no dependencies

This repository is an example of creating a very simple REST API in go using only the standard packages

## How to use

Just simple run:

`go run server.go`

in order to start the server

`CTRL + c`

will shut it down